---
title: "Fallback Sample"
---

# Fallback Test

This is a fallback test for a page that was never defined in any specific language. It should be available in all languages throughout the menu and be displayed in the language it was created in.